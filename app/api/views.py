from django.shortcuts import render
from django.http import HttpResponse
from django.views import View
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from knox.models import AuthToken
from rest_framework import generics, permissions, viewsets
from rest_framework.response import Response
import json
import logging
from .models import Event, Token, Entry
from .serializres import CreateUserSerializer, LoginUserSerializer, UserSerializer, EventSerializer, TokenSerializer, EntrySerializer

@method_decorator(csrf_exempt, name='dispatch')
class AnonymousEntryView(View):
    def post(self, request, *args, **kwargs):
        entry = Entry(name=request.POST['name'])
        entry.token = Token.objects.get(code=request.POST['token'])
        entry.save()
        data = {
            'event':entry.token.event.title
        }
        return HttpResponse(json.dumps(data), content_type='application/json', status=200)

@method_decorator(csrf_exempt, name='dispatch')
class TokenCreateView(View):
    def post(self, request, *args, **kwargs):
        data = json.loads(request.body.decode('utf-8'))
        logging.info("Token creating... data=" + str(data))
        event = Event.objects.get(pk=data['event'])
        token = Token()
        token.event = event
        token.save()
        logging.info("Token created. code=" + token.code)
        data = {
            'code': token.code
        }
        return HttpResponse(json.dumps(data), content_type='application/json', status=200)

class TokenList(generics.ListAPIView):
    serializer_class = TokenSerializer

    def get_queryset(self):
        eid = self.kwargs['eid']
        return Token.objects.filter(event=Event.objects.get(pk=eid))

class EntryList(generics.ListAPIView):
    serializer_class = TokenSerializer

    def get_queryset(self):
        tid = self.kwargs['tid']
        return Entry.objects.filter(token=Token.objects.get(pk=tid))

class TokenViewset(viewsets.ModelViewSet):
    queryset = Token.objects.all()
    permission_classes = [permissions.IsAuthenticated, ]
    serializer_class = EventSerializer

class EventViewset(viewsets.ModelViewSet):
    queryset = Event.objects.all()
    permission_classes = [permissions.IsAuthenticated, ]
    serializer_class = EventSerializer

    def get_queryset(self):
        return self.request.user.entries.all()
    
    def perform_create(self, serializer):
        print(self.request.POST)
        serializer.save(owner=self.request.user)

class RegistrationAPI(generics.GenericAPIView):
    serializer_class = CreateUserSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        return Response({
            "user": UserSerializer(user, context=self.get_serializer_context()).data,
            "token": AuthToken.objects.create(user)[1]
        })


class LoginAPI(generics.GenericAPIView):
    serializer_class = LoginUserSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data
        return Response({
            "user": UserSerializer(user, context=self.get_serializer_context()).data,
            "token": AuthToken.objects.create(user)[1]
        })


class UserAPI(generics.RetrieveAPIView):
    permission_classes = [permissions.IsAuthenticated, ]
    serializer_class = UserSerializer

    def get_object(self):
        return self.request.user