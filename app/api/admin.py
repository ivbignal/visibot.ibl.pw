from django.contrib import admin
from .models import Event, Token, Entry

admin.site.register(Event)
admin.site.register(Token)
admin.site.register(Entry)
