from django.urls import path, include
from rest_framework import routers
from . import views

router = routers.DefaultRouter()
router.register('event', views.EventViewset, 'event')
router.register('token', views.TokenViewset, 'token')

urlpatterns = [
    path('auth/register/', views.RegistrationAPI.as_view()),
    path('auth/login/', views.LoginAPI.as_view()),
    path('auth/user/', views.UserAPI.as_view()),
    path('auth/', include('knox.urls')),
    path('anonymousentry/', views.AnonymousEntryView.as_view()),
    path('tokencreate/', views.TokenCreateView.as_view()),
    path('tokenlist/<eid>/', views.TokenList.as_view()),
    path('entrylist/<eid>/', views.TokenList.as_view()),
    path('', include(router.urls)),

    # path('entries/', views.EntryViews.ListView.as_view()),
    # path('entries/create/', views.EntryViews.CreateView.as_view()),
    # path('entries/<int:pk>/', views.EntryViews.DetailView.as_view()),
    # path('tokens/', views.TokenViews.ListView.as_view()),
    # path('tokens/create/', views.TokenViews.CreateView.as_view()),
    # path('tokens/<int:pk>/', views.TokenViews.DetailView.as_view()),
    # path('events/', views.EventViews.ListView.as_view()),
    # path('events/create/', views.EventViews.CreateView.as_view()),
    # path('events/<int:pk>/', views.EventViews.DetailView.as_view()),
]
