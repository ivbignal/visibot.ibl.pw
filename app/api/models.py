from django.db import models
from django.contrib.auth.models import User
from django.utils.crypto import get_random_string

class Event(models.Model):
    title = models.CharField(verbose_name='Event title', max_length=64)
    owner = models.ForeignKey(User, related_name="entries",
                              on_delete=models.CASCADE, null=True)

def rndToken():
    token = get_random_string(6, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789')
    print('Generated new token: ' + token)
    return token

class Token(models.Model):
    code = models.CharField(verbose_name='Token code', null=True, blank=True, unique=True, max_length=10, default=rndToken)
    date = models.DateTimeField(verbose_name='Token creation date', auto_now=True)
    event = models.ForeignKey(Event, verbose_name='Token event', related_name='tokens',
     null=False, blank=False, on_delete=models.CASCADE)


class Entry(models.Model):
    name = models.CharField(verbose_name='Student name', max_length=64)
    date = models.DateTimeField(verbose_name='Token creation date', auto_now=True)
    token = models.ForeignKey(Token, verbose_name='Entry token', related_name='entries',
     null=False, blank=False, on_delete=models.CASCADE)
    
    class Meta:
        verbose_name = 'Entry'
        verbose_name_plural = 'Entries'
    