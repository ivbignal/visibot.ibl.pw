import React, { Component } from "react";
import ReactDOM from 'react-dom';
import Login from "./components/Login";
import Register from './components/Register';
import { Provider } from 'react-redux';
import { connect } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import thunk from 'redux-thunk';
import { Switch, Route, BrowserRouter, Redirect } from 'react-router-dom';
import vibotApp from './reducers';
import { auth } from './actions';
import Eventslist from "./components/Eventslist";
import Tokenslist from "./components/Tokenslist";
import Entrylist from "./components/Entrylist";

let store = createStore(vibotApp, applyMiddleware(thunk));

class RootContainerComponent extends Component {

  componentDidMount() {
    this.props.loadUser();
  }

  PrivateRoute = ({ component: ChildComponent, ...rest }) => {
    return <Route {...rest} render={props => {
      if (this.props.auth.isLoading) {
        return <em>Loading...</em>;
      } else if (!this.props.auth.isAuthenticated) {
        return <Redirect to="/login" />;
      } else {
        return <ChildComponent {...props} />
      }
    }} />
  }

  render() {
    let { PrivateRoute } = this;
    return (
      <BrowserRouter>
        <Switch>
          <PrivateRoute exact path="/" component={MainPage} />
          <PrivateRoute exact path="/event/:eventId/tokens" component={Tokenslist} />
          <PrivateRoute exact path="/token/:tokenId/entries" component={Entrylist} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/register" component={Register} />
          <PrivateRoute component={NotFound} />
        </Switch>
      </BrowserRouter>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth: state.auth,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    loadUser: () => {
      return dispatch(auth.loadUser());
    }
  }
}

let RootContainer = connect(mapStateToProps, mapDispatchToProps)(RootContainerComponent);

class NotFound extends Component {
  render() {
    return (
      <h1>Not Found</h1>
    )
  }
}

class MainPageComponent extends Component {
  render() {
    return (
      <div>
        <h1>Добро пожаловать<br />в&nbsp;Посещабот!&nbsp;:)</h1>
        Вы вошли как <b>{this.props.user.username}</b> - <button onClick={this.props.logout}>выйти</button>
          <br />
        <Eventslist />
      </div>
    )
  }
}

const mapStateToPropsmain = state => {
  return {
    user: state.auth.user,
  }
}

const mapDispatchToPropsmain = dispatch => {
  return {
    logout: () => dispatch(auth.logout()),
  }
}

let MainPage = connect(mapStateToPropsmain, mapDispatchToPropsmain)(MainPageComponent);

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <RootContainer />
      </Provider>
    );
  }
}

export default App;

ReactDOM.render(<App />, document.getElementById("app"));