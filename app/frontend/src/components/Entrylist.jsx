import React, { Component } from "react";
import { connect, Provider } from "react-redux";

import { Link, Redirect } from "react-router-dom";

import { tokens, entries } from '../actions';
// import store from '../store/token'


class Entrylist extends Component {
    
    componentDidMount() {
        this.props.fetchEntries(this.props.match.params.tokenId);
    }
    
    render() {
        // if (this.props.events.length < 1) return <Redirect to="/" />
        // else
        return (
                <div>
                    <h2>Nova.IN</h2>
                    <hr />
                    <h3>tokens list</h3>
                    <table>
                        <tbody>
                            {this.props.entries.map((entry, id) => (
                                <tr key={`note_${id}`}>
                                    <td>{entry.name}</td>
                                </tr>
                            ))}
                        </tbody>

                    </table>
                </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        entries: state.entries.r_obj,
        tokens: state.tokens.r_obj,
    };
}

const mapDispatchToProps = dispatch => {
    return {
        fetchEntries: (tId) => {
            dispatch(entries.fetchall(tId));
        },
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Entrylist);