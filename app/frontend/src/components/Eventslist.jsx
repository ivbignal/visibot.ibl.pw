import React, { Component } from "react";
import { connect } from "react-redux";

import { Link, Redirect } from "react-router-dom";

import { events } from '../actions';


class Eventslist extends Component {
    state = {
        title: "",
        updateEventId: null,
    }

    resetForm = () => {
        this.setState({ title: "", updateEventId: null });
    }

    selectForEdit = (id) => {
        let event = this.props.events[id];
        this.setState({ title: event.title, updateEventId: id });
    }

    deleteEvent = (id) => {
        this.props.deleteEvent(id);
    }

    submitEvent = (e) => {
        e.preventDefault();
        if (this.state.updateEventId === null) {
            this.props.addEvent(this.state.title);
        } else {
            this.props.updateEvent(this.state.updateEventId, this.state.title);
        }
        this.resetForm();
    }

    componentDidMount() {
        this.props.fetchevents();
    }

    render() {
        return (
            <div>
                <br/><br/>
                <h2>Мои события</h2>
                <p>Здесь можно создавать события и отслеживать посещаемость уже созданных!</p>
                <hr />

                <h3>Новое событие</h3>
                <form onSubmit={this.submitEvent}>
                    <tr>
                        <td>
                            <input
                                value={this.state.title}
                                placeholder="Название события..."
                                onChange={(e) => this.setState({ title: e.target.value })}
                                required />
                        </td>
                        <td>
                            <input type="submit" value="Добавить / Сохранить изменения" />
                        </td>
                        <td>
                            <button onClick={this.resetForm}>Сброс</button>
                        </td>
                    </tr>
                </form>
                <hr/>
                <h3>Список событий</h3>
                <table>
                    <tbody>
                        {this.props.events.map((event, id) => (
                            <tr key={`e_${id}`}>
                                <td>{event.title}</td>
                                <td><button onClick={() => this.selectForEdit(id)}>Редактировать</button></td>
                                <td><button onClick={() => this.deleteEvent(id)}>Удалить</button></td>
                                <td><button onClick={() => window.location = `/event/${event.id}/tokens`}>Токены</button></td>
                            </tr>
                        ))}
                    </tbody>

                </table>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        events: state.events.r_obj,
    };
}

const mapDispatchToProps = dispatch => {
    return {
        addEvent: (title) => {
            dispatch(events.add(title));
        },
        updateEvent: (id, title) => {
            dispatch(events.update(id, title));
        },
        deleteEvent: (id) => {
            dispatch(events.del(id));
        },
        fetchevents: () => {
            dispatch(events.fetchall());
        },
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Eventslist);