import React, { Component } from "react";
import { connect, Provider } from "react-redux";
import Modal from 'react-modal';

import { Link, Redirect } from "react-router-dom";

import { tokens, events } from '../actions';
// import store from '../store/token'


class Tokenslist extends Component {

    state = {
        openModal: -1,
        loaded: false
    }

    submitToken = (e) => {
        e.preventDefault();
        this.props.addToken(this.props.match.params.eventId);
        window.location.reload();
    }
    
    componentDidMount() {
        console.log(this.props.fetchTokens(this.props.match.params.eventId))
        // .then(res => {
            console.log('loaded')
            // this.setState({loaded: true})
        // });
    }

    open(id) {

    }

    render_page() {
        return (
                <div>
                    <td><button onClick={() => window.location = `/`}>Назад к списку событий</button></td>
                    <h2>Токены события</h2>
                        <p>Здесь можно создавать токены события и отслеживать посещаемость по ранее созданным!</p>
                        <hr />

                    <form onSubmit={this.submitToken}>
                        <input type="submit" value="Новый токен!" />
                    </form>
                    <h3>Список токенов</h3>
                    <table>
                        <tbody>
                            {this.props.tokens && this.props.tokens.map((token, id) => (
                                <tr key={`note_${id}`}>
                                    <td>{token.code}</td>
                                    <td>{token.date}</td>
                                    <td><button onClick={() => this.props.deleteToken(id)}>Удалить</button></td>
                                    <td><button onClick={() => this.setState({openModal: id})}>Записи посещаемости</button></td>
                                    <Modal
                                        isOpen={this.state.openModal == id}
                                    >
                                        <tr>
                                            <td>Записи по токену {token.code} (от {token.date})</td>
                                            <td><button onClick={() => this.setState({openModal: -1})}>Закрыть</button></td>
                                        </tr>
                                        
                                            {token.entries.map((entry, eid) => (
                                                <tr key={`entry_${id}_${eid}`}>
                                                        <td>{entry.name}</td>
                                                        <td>{entry.date}</td>
                                                </tr>
                                            ))}
                                    </Modal>
                                </tr>
                            ))}
                        </tbody>

                    </table>
                </div>
        )
    }
    
    render() {
        console.log(this.props.tokens.length);
        const { loaded } = this.state
        return this.props.tokens ? this.render_page() : (<h1>Loading...</h1>)
    }
}

const mapStateToProps = state => {
    return {
        tokens: state.tokens.r_obj,
        events: state.events.r_obj,
    };
}

const mapDispatchToProps = dispatch => {
    return {
        addToken: (event) => {
            dispatch(tokens.add(event));
        },
        deleteToken: (id) => {
            dispatch(tokens.del(id));
        },
        fetchTokens: (eId) => {
            dispatch(tokens.fetchall(eId));
        },
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Tokenslist);