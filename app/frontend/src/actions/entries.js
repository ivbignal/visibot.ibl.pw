import { getCookie } from '../utils';

export const fetchall = (tId) => {
    return (dispatch, getState) => {
        let headers = { "Content-Type": "application/json" };
        let { token } = getState().auth;
        if (token) {
            headers["Authorization"] = `Token ${token}`;
        }
        console.log("EID:", tId);
        return fetch("/api/v1/entrylist/" + tId + "/", { headers, })
            .then(res => {
                if (res.status < 500) {
                    return res.json().then(data => {
                        return { status: res.status, data };
                    })
                } else {
                    console.log("Server Error!");
                    throw res;
                }
            })
            .then(res => {
                if (res.status === 200) {
                    return dispatch({ type: 'FETCH', payload: res.data });
                } else if (res.status === 401 || res.status === 403) {
                    dispatch({ type: "AUTHENTICATION_ERROR", payload: res.data });
                    throw res.data;
                }
            })
    }
}