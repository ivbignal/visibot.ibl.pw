import { getCookie } from '../utils';

export const add = event => {
    return (dispatch, getState) => {
      let headers = {"Content-Type": "application/json"};
      let {token} = getState().auth;
      
      if (token) {
        headers["Authorization"] = `Token ${token}`;
      }

      headers['X-CSRFToken'] = getCookie('csrftoken');
      headers["X-Requested-With"] = "XMLHttpRequest";
      console.log("Got event: ", event);
  
      let body = JSON.stringify({event});
      console.log('Created body: ', body);
      return fetch("/api/v1/tokencreate/", {headers, method: "POST", body})
        .then(res => {
          if (res.status < 500) {
            return res.json().then(data => {
              return {status: res.status, data};
            })
          } else {
            console.log("Server Error!");
            throw res;
          }
        })
        .then(res => {
          if (res.status === 201) {
            return dispatch({type: 'ADD', payload: res.data});
          } else if (res.status === 401 || res.status === 403) {
            dispatch({type: "AUTHENTICATION_ERROR", data: res.data});
            throw res.data;
          }
        })
    }
  }
  
  export const del = index => {
    return (dispatch, getState) => {
  
      let headers = {"Content-Type": "application/json"};
      let {token} = getState().auth;
  
      if (token) {
        headers["Authorization"] = `Token ${token}`;
      }
  
      let categoryId = getState().tokens.r_obj[index].id;
  
      return fetch(`/api/v1/token/${categoryId}/`, {headers, method: "DELETE"})
        .then(res => {
          if (res.status === 204) {
            return {status: res.status, data: {}};
          } else if (res.status < 500) {
            return res.json().then(data => {
              return {status: res.status, data};
            })
          } else {
            console.log("Server Error!");
            throw res;
          }
        })
        .then(res => {
          if (res.status === 204) {
            return dispatch({type: 'DELETE', index});
          } else if (res.status === 401 || res.status === 403) {
            dispatch({type: "AUTHENTICATION_ERROR", payload: res.data});
            throw res.data;
          }
        })
    }
  }

export const fetchall = (eId) => {
    return (dispatch, getState) => {
        let headers = { "Content-Type": "application/json" };
        let { token } = getState().auth;
        if (token) {
            headers["Authorization"] = `Token ${token}`;
        }
        console.log("EID:", eId);
        return fetch("/api/v1/tokenlist/" + eId + "/", { headers, })
            .then(res => {
                if (res.status < 500) {
                    return res.json().then(data => {
                        return { status: res.status, data };
                    })
                } else {
                    console.log("Server Error!");
                    throw res;
                }
            })
            .then(res => {
                if (res.status === 200) {
                    return dispatch({ type: 'FETCH', payload: res.data });
                } else if (res.status === 401 || res.status === 403) {
                    dispatch({ type: "AUTHENTICATION_ERROR", payload: res.data });
                    throw res.data;
                }
            })
    }
}