const initialState = {
    obj:[],
    r_obj:[]
};

export default function entries(state = initialState, action) {
    switch (action.type) {
        case 'FETCH':
            return Object.assign({}, state, {
                r_obj: action.payload
              });
    }
    return state;
}