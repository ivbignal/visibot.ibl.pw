import { combineReducers } from 'redux';
import auth from "./auth";
import events from "./events";
import tokens from "./tokens";
import entries from "./entries";

const vibotApp = combineReducers({
    auth, events, tokens, entries
})

export default vibotApp