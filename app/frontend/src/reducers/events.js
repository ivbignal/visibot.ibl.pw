const initialState = {
    obj:[],
    r_obj:[]
};

export default function events(state = initialState, action) {
    switch (action.type) {
        case 'ADD':
            return Object.assign({}, state, {
                r_obj: state.r_obj.concat(action.payload)
              });

        case 'UPDATE':
            var o = state.r_obj.slice();
            o.splice(action.index, 1, action.payload)
            return Object.assign({}, state, {
                r_obj: o
              });

        case 'DELETE':
            var o = state.r_obj.slice();
            o.splice(action.index, 1)
            return Object.assign({}, state, {
                r_obj: o
              });
        
        case 'FETCH':
            return Object.assign({}, state, {
                r_obj: action.payload
              });

        default:
            return state;
    }
}